#include <cassert>
#include "liba.hpp"

void test_version() {
	assert(liba::version() == "1.0");
}

void test_point2d() {
	using liba::point2d;

	point2d p{1.0, 1.0};
	assert(p.x == 1.0 && p.y == 1.0);
}

int main(int argc, char * argv[]) {
	test_version();
	return 0;
}

#pragma once

#if __cplusplus >= 201703L
#include <string_view>
#else
#include <string>
#endif

namespace liba {

int meaning_of_life();
double version_as_num();
bool with_cpp17_support();

struct point2d {
	double x, y;
};

#if __cplusplus >= 201703L

std::string_view version();

#else

std::string const & version();

#endif

}  // liba

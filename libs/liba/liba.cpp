#include "liba.hpp"

namespace liba {

int meaning_of_life() {return 42;}
double version_as_num() {return 1.0;}

bool with_cpp17_support() {
#if __cplusplus >= 201703L
	return true;
#else
	return false;
#endif
}

#if __cplusplus >= 201703L

using std::string_view;
using namespace std::literals;

string_view version() {
	return "1.0"sv;
}

#else

using std::string;

string const & version() {
	static string const ver = "1.0";
	return ver;
}

#endif

}  // liba

#include <iostream>
#include "liba/liba.hpp"
using std::cout, std::cerr;

int main(int argc, char * argv[]) {
	if (liba::version_as_num() < 1.0) {
		cerr << "unsupported library version " << liba::version_as_num() << ", we expect at least 1.0 version\n";
		return 1;  // unsupported library version
	}

	cout << "the meaning of life is " << liba::meaning_of_life() << "\n"
		<< "powered by liba:" << liba::version() 
			<< " (" << (liba::with_cpp17_support() ? "with" : "without") << " C++17 support)\n";

	return 0;
}
